#include "BSNode.h"
#include <iostream>

using namespace std;


#define DONT_WORK -11111111

BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}


BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other.getCount();
}


BSNode::~BSNode()
{
	if (this->_left != nullptr)
	{
		delete (this->_left);
	}

	if (this->_right != nullptr)
	{
		delete (this->_right);
	}
}


string BSNode::getData() const
{
	return this->_data;
}


int BSNode::getCount() const
{
	return this->_count;
}


bool BSNode::isLeaf() const
{
	return ((this->_left == nullptr && this->_right == nullptr) ? true : false);
}



BSNode* BSNode::getLeft() const
{
	return this->_left;
}


BSNode* BSNode::getRight() const
{
	return this->_right;
}



void BSNode::insert(string value)
{

	if ((std::stoi(value) < std::stoi(this->_data) && this->_left == nullptr) || std::stoi(value) == std::stoi(this->_data))
	{
		if (std::stoi(value) == std::stoi(this->_data))
		{
			this->_count++;
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else if (std::stoi(value) < std::stoi(this->_data) || std::stoi(value) == std::stoi(this->_data))
	{
		if (std::stoi(value) == std::stoi(this->_data))
		{
			this->_count++;
		}
		else
		{
			this->_left->insert(value);
		}

	}
	else if (std::stoi(value) > std::stoi(this->_data) && this->_right == nullptr)
	{
		this->_right = new BSNode(value);
	}
	else
	{
		this->_right->insert(value);
	}
}


bool BSNode::search(string val) const
{
	bool isFound = false;

	if (std::stoi(val) > std::stoi(this->_data))
	{
		if (this->_right == nullptr)
		{
			isFound = false;
		}
		else
		{
			isFound = this->_right->search(val);
		}
	}
	else if (std::stoi(val) < std::stoi(this->_data))
	{
		if (this->_left == nullptr)
		{
			isFound = false;
		}
		else
		{
			isFound = this->_left->search(val);
		}
	}
	else if (std::stoi(val) == std::stoi(this->_data))
	{
		isFound = true;
	}
	return isFound;
}


BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode* newBSNode = new BSNode(other.getData());
	return *newBSNode;
}



int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (std::stoi(this->getData()) == std::stoi(node->getData()))
	{
		return 0;
	}
	else if (std::stoi(this->getData()) < std::stoi(node->getData()) && this->_right != nullptr)
	{
		return 1 + this->_right->getCurrNodeDistFromInputNode(node);
	}
	else if (std::stoi(this->getData()) > std::stoi(node->getData()) && this->_right != nullptr)
	{
		return 1 + this->_left->getCurrNodeDistFromInputNode(node);
	}
	else
	{
		return DONT_WORK;
	}
}



int BSNode::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}


int BSNode::getHeight() const
{
	int leftH = 0;
	int rightH = 0;;

	if (this->_left != nullptr)
	{
		leftH = this->_left->getHeight();
	}

	if (this->_left == nullptr)
	{
		leftH = -1;
	}

	if (this->_right != nullptr)
	{
		rightH = this->_right->getHeight();
	}
	else
	{
		rightH = -1;
	}

	return ((leftH > rightH) ? (leftH + 1) : (rightH + 1));
}


void BSNode::printNodes() const
{
	//first recur on left child
	if (this->_left != nullptr)
	{
		this->_left->printNodes();
	}
	//then print the data of node
	std::cout << "The data >>> " << this->_data << " appears: " << this->_count << " times" << std::endl;

	//now recur on right child
	if (this->_right != nullptr)
	{
		this->_right->printNodes();
	}
}


