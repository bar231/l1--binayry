﻿#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>



#include "printTreeToFile.h"
#include "BSNode.h"


using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");

	BSNode* newBs = new BSNode("1");

	newBs->insert("1");
	newBs->insert("2");
	newBs->insert("2");
	newBs->insert("3");
	newBs->insert("3");
	newBs->insert("4");
	newBs->insert("4");
	newBs->insert("5");
	newBs->insert("5");

	std::cout << "print the nodes!!" << std::endl;
	newBs->printNodes();

	cout << "starting" << endl;

	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	cout << "is leaf? " << bs->isLeaf() << endl;
	cout << "is in the tree? " << bs->search("1") << endl;


	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "the data is: " << bs->getLeft()->getRight()->getRight()->getData() << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;

	std::cout << "height is: " << bs->getHeight() << std::endl;

	std::cout << "print the nodes!!" << std::endl;
	bs->printNodes();

	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	remove(textTree.c_str());
	delete bs;

	system("pause");
	return 0;

}