#pragma once
#include <iostream>

class Example
{
public:
	Example(int number);
	~Example();
	bool operator<(const Example& other) const;
	bool operator>(const Example& other) const;
	bool operator==(const Example& other) const;
	friend std::ostream& operator<<(std::ostream& os, const Example& dt);
	Example& operator=(const Example& other);

private:
	int _number;
};

template <class TYPE>
int compare(TYPE first, TYPE second)
{
	int sum = 0;
	if (first == second)
	{
		sum = 0;
	}
	else if (first < second)
	{
		sum = 1;
	}
	else
	{
		sum = -1;
	}
	return sum;
}


template <class Y>
void bubbleSort(Y* arr, int size)
{
	Y object(1);
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				object = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = object;
			}
		}
	}
}


template <class Z>
void printArray(Z* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << "object in [" << i << "] place is: " << arr[i] << std::endl;
	}
}