#include "Functions.h"

Example::Example(int number)
{
	this->_number = number;
}

Example::~Example()
{
}

bool Example::operator<(const Example& other) const
{
	return this->_number < other._number;
}


bool Example::operator>(const Example& other) const
{
	return this->_number > other._number;
}



bool Example::operator==(const Example& other) const
{
	return this->_number == other._number;
}


std::ostream& operator<<(std::ostream& os, const Example& dt)
{
	os << dt._number;
	return os;
}


Example& Example::operator=(const Example& other)
{
	this->_number = other._number;
	return *this;
}